\documentclass[11pt]{article}

\usepackage[T1]{fontenc}
\usepackage{mathptmx}
\usepackage{graphicx}

\topmargin 0.0in
\setlength{\textwidth} {420pt}
\setlength{\textheight} {620pt} 
\setlength{\oddsidemargin} {20pt}
\setlength{\marginparwidth} {72in}

\usepackage{fancyhdr} 
\usepackage{url}

% set it so that subsubsections have numbers and they
% are displayed in the TOC (maybe hard to read, might want to disable)

\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

% define widow protection

\def\widow#1{\vskip #1\vbadness10000\penalty-200\vskip-#1}

\clubpenalty=10000  % Don't allow orphans
\widowpenalty=10000 % Don't allow widows

% this should give me the ability to use some math symbols that 
% were available by default in standard latex (i.e. \Box)

\usepackage{latexsym}

% define a little section heading that doesn't go with any number

\def\littlesection#1{
\widow{2cm}
\vskip 0.5cm
\noindent{\bf #1}
\vskip 0.0001cm 
}

\pagestyle{fancyplain}

\newcommand{\tstamp}{\today}   
\renewcommand{\sectionmark}[1]{\markright{#1}}
\lhead[\Section \thesection]            {\fancyplain{}{\rightmark}}
\chead[\fancyplain{}{}]                 {\fancyplain{}{}}
\rhead[\fancyplain{}{\rightmark}]       {\fancyplain{}{\thepage}}
\cfoot[\fancyplain{\thepage}{}]         {\fancyplain{\thepage}{}}

\newlength{\myVSpace}% the height of the box
\setlength{\myVSpace}{1ex}% the default, 
\newcommand\xstrut{\raisebox{-.5\myVSpace}% symmetric behaviour, 
  {\rule{0pt}{\myVSpace}}%
}

% leave things with no spacing extra spacing in the final version of the paper
\renewcommand{\baselinestretch}{1.0}    % must go before the begin of doc

% suppress the use of indentation for a paragraph

\setlength{\parindent}{0.0in}
\setlength{\parskip}{0.1in}

\begin{document}

%% \begin{abstract}

%%   Try

%% \end{abstract}

% handle widows appropriately
\def\widow#1{\vskip #1\vbadness10000\penalty-200\vskip-#1}

% build the title section

\makeatletter

\def\maketitle{%
  %\null
  \thispagestyle{empty}%
  %\vfill
  \begin{center}%\leavevmode
    %\normalfont
    {\Huge \@title\par}%
    %\hrulefill\par
    {\normalsize \@author\par}%
    \vskip .4in
%    {\Large \@date\par}%
  \end{center}%
  %\vfill
  %\null
  %\cleardoublepage

  }

\makeatother

\vspace*{-1.1in}
\title{A Genetic Algorithm Approach to Modeling the Human Visual System with Respect to Advertisements}

% build the author section
\author{Timothy J. Casto\\
Department of Computer Science\\
Allegheny College \\
{\tt castot@allegheny.edu}  \\
\vspace*{.1in} \today \\ \vspace*{.1in}
{\bf Abstract} \\ Using eye-tracking equipment, participants will be tracked while viewing different types of advertisements, with different goals in mid while they view the ads. After the data from the eye tracking is collected, a genetic algorithm will be developed to attempt to model how humans view the advertisements, by producing heat maps for all of the advertisements viewed by the participants. It is expected that people viewing advertisements with a search-based goal in mind will remember the information from the ads, more than those without a search-based goal. Additionally, it is expected that people viewing print ads will have a better memory for the information than people viewing Internet banners because of the concept known as Banner Blindness. }

% use the default title stuff
\maketitle

\vspace*{-.4in}
\section{Introduction}
\label{sec:introduction}
\vspace*{-.1in}

Can a genetic algorithm (GA) be used to model the way the human visual system would process advertisements, with different search goals in mind? Genetic algorithms have been used to find solutions to problems that would be impossible or impractical for people to solve on their own, such as many NP-Complete problems which will be discussed later in the paper. These methods to solving complex problems may also be able to predict where people will look at advertisements. Companies and advertisers have very limited time and space to sell their product to consumers, whether it be in television commercials, magazine ads, or banners on the sides of websites. Due to these limitations it is important for companies to know where consumers are looking when they view the advertisements. Are the important parts of the ads being attended to, or are people looking at completely different spots?

Eye-tracking equipment can be used to map where people look at images. This would allow advertisers to see exactly what parts of the ads people are focusing on, and make adjustments to the ads if necessary. There are several problems with using this eye-tracking approach to testing the advertisements. The equipment is expensive, it takes training to use the equipment, and analyze the results, and only one person can be tested at a time. If a genetic algorithm could accurately predict where people will look for different advertisements, all of these problems would be solved, and the same results could be achieved.  

The rest of the paper will be organized as follows: first, an explanation of genetic algorithms will be provided, then a review of current research will be given. Next, ways that GA's can be applied to the proposed research will be examined, and finally the methods for the study will be examined. 

\vspace*{-.1in}
\section{Eye-Tracking}
\label{sec:eyetracking}
\vspace*{-.1in}

Using eye-tracking equipment, researchers can track saccades and fixations as a person looks at an image. With this information a heat-map can be generated to show where people tend to look most on the image, and for how long, as well as the visual paths they took to get from one place to another. This method of eye tracking can also be used to track which parts of an advertisement people tend to focus on the most, as well as which parts are ignored. With this information, advertisers can adjust advertisements to make the chances of people getting the desired information increase. This increased chance of people noticing information about the advertisement is extremely valuable for companies who spend a large amount of money producing advertisements for their products. The present study uses the ASL Eye-Trac 6 Series Monocular eye-tracker at Allegheny College. 

\vspace*{-.1in}
\section{Genetic Algorithms}
\label{sec:genetic}
\vspace*{-.1in}

Can a genetic algorithm (GA) be used to model the way the human visual system would process advertisements, with different search goals in mind? Genetic algorithms have been used to find solutions to problems that would be impossible or impractical for people to solve on their own, such as many NP-Complete problems which will be discussed later in the paper. These methods to solving complex problems may also be able to predict where people will look at advertisements. Companies and advertisers have very limited time and space to sell their product to consumers, whether it be in television commercials, magazine ads, or banners on the sides of websites. Due to these limitations it is important for companies to know where consumers are looking when they view the advertisements. Are the important parts of the ads being attended to, or are people looking at completely different spots?

Eye-tracking equipment can be used to map where people look at images. This would allow advertisers to see exactly what parts of the ads people are focusing on, and make adjustments to the ads if necessary. There are several problems with using this eye-tracking approach to testing the advertisements. The equipment is expensive, it takes training to use the equipment, and analyze the results, and only one person can be tested at a time. If a genetic algorithm could accurately predict where people will look for different advertisements, all of these problems would be solved, and the same results could be achieved.  

Genetic Algorithms are solutions to problems, which use biologically inspired principles such as evolution and survival of the fittest to solve problems that would otherwise be very difficult to solve, or far too time consuming. A classic example of a problem that can be solved using a GA, is called the Knapsack Problem. In this problem, you are given a knapsack that can hold up to a certain weight, and a group of items, as well as their weights and values/prices. The idea is that you want to maximize the value of the items in your knapsack. This sounds very easy when presented with just a few items, but say the list is 100 items long, with price and weigh ranges varying significantly. Very quickly, the problem grows out of hand, and the number of possible configurations for the knapsack becomes extremely large. The Knapsack Problem is a trivial example of the use of a genetic algorithm, but it is a simple enough one to understand. This type of problem falls under the category of an NP-Complete problem in Computer Science. NP-Complete refers to the idea that a solution to the problem can be verified very easily, in polynomial time, but there is no fast and efficient way of solving the problem itself. A problem being solvable or verifiable in polynomial time means that the time it takes to solve or check a solution can be expressed as a polynomial expression, e.g. some variable X raised to a constant K. 

Computer scientists use genetic algorithms as an approach to solving NP-Complete problems because of the nature of how the algorithms themselves work. First, in a GA, the programmer must define the fitness of a solution. In other words, a solution is more or less ``fit'' based on how good of a solution it is for the problem. Due to the nature of the types of problems GA's are generally applied to, it is often impossible to know what the best solution to the problem is, if that is even possible to determine at all. Therefore a most-fit approach to solving the problems is often taken. Looking back at the Knapsack Problem, a ``fit'' solution would be one that comes as close as possible to the maximum weight (without going over), as well as maximizing the value of the objects in the bag. A solution that wouldn't be fit at all would be one that exceeds the maximum weight, or one that stays within the weight restrictions, but has a low overall value. 

Once the fitness function is developed, the programmer decides what type of elimination, crossover and mutation effects they want to use. There are a wide range of choices here, so just a few will be examined. Tournament style elimination involves having several possible solutions being grouped together, and only the most fit one survives. In the context of the Knapsack Problem, this would look like several possible solutions being put into a group randomly, and then the fitness of each solution in the group is checked. Only the solution with the best fitness among these is kept, and the rest are discarded. This will happen with many different initial groups, and then it can be repeated on the winners. Winners will advance and be stored as possible solutions. Another option is to take two solutions called ``parents'' and combine them. In the case of the Knapsack problem, two different solutions could be examined, and elements of both could be combined to produce children, which would then be determined to be fit or unfit. Mutation is the idea that every so often a solution that advances will have some small element of it randomly changed. For example, in the Knapsack Problem, an item could randomly be replaced with another item. This may make it more or less fit, but it adds an element of randomness to the algorithm, just like in nature. 

Once the fitness function, crossover, elimination and mutation elements have been decided on, the programmer must determine how and when the algorithm will stop. One choice is to stop after a predetermined number of generations are developed, without any new, most fit solutions showing up. This plateau suggests that the current solution is probably going to be the best that the algorithm can develop. That being said, it is not a guarantee that it is the best possible solution to the problem itself. The fitness function could be flawed due to the programmer picking a poor representation of what is fit for a solution to the problem.

\vspace*{-.1in}
\section{Related Work}
\label{sec:relatedwork}
\vspace*{-.1in}

Advertising is a huge industry, that billions of dollars are spent on yearly. In 2008, \$28 billion was spent on internet advertisement alone \cite{Hervet}. Given that so much money is spent on advertising every year, the effectiveness of advertisements is certainly of concern to many ad designers. Eye-tracking researchers might expect that eye-tracking would be a popular choice for testing ads, but as it turns out not many advertising companies currently test their ads with eye-tracking. This is likely due to how expensive the equipment is, that training to use the equipment is required, and that only one person can be tracked at a time. In fact, surveys tend to be the most popular method of testing ads, because of the ease with which they can be administered, and the cheap cost of the tests \cite{Berger}. If a  more cost effective method of advertisement testing can be produced using a computer program, that does not rely on participants reports in surveys, the market for that new type of testing would have little competition. This study will provide a user friendly, cost effective alternative program to simulate how people will view advertisements. Surveys depend heavily on participant self-reports, which can be influenced by many factors, such as mood, expectations (of both the participants and the researchers), and the question format of the surveys themselves. The goals of the viewers, going into the ad viewing session, can also impact how they look at the ads themselves. Being instructed to search for certain aspects of the advertisement, such as the brand, can influence which parts of the ad people focus on and remember \cite{Pieters02}. This leads one to question the methods of evaluating advertisements through surveys in the first place.

There is currently little work being done to attempt to combine eye-tracking and genetic algorithms, but the inspiration for the present study comes from research done by Zhang, Hong, Zhen, Xiaoyu, Zheru, Dagan, and Xinbo (2009). In their paper, the authors looked specifically at obvious-object and non-obvious-object scenes. For example, an obvious-object scene might contain a picture of a fish with a black background, or a face on a white background. In these images, it is obvious what the focus of the image is. In contrast, in the non-obvious-object images, one might find a picture of a city block, or a garden. Here, there is no specific focus of the image to which a person's attention will be drawn \cite{4983060}.

Zhang and Siyuan (2007) aimed to speed up the process of using genetic algorithms to solve problems by changing how the selection and crossover methods operated. When selecting parents to produce children, the algorithm they created used a local optimum search to determine what parents would be best to produce offspring \cite{RZhang}. This effectively reduced the number of parents used, but aimed to make the ones that were used better. A local optimum search is pretty straight forward, the algorithm checks the nearby solutions to see which ones are the most fit, or the best solutions, and then takes the ones it finds and uses them for the crossover and child creation. This idea of speeding up the genetic algorithm is specifically useful to the present study in that a faster and more efficient solution is desirable. If the algorithm takes hours, or even days to run, this could have real world implications as to the usefulness of the program outside of research. Perhaps the best part of the implementation of this local optimum approach is the simplicity of it. Incorporating a local maximum search into the algorithm, before parent selection, is easy to do, and if it ended up not working, it could be removed without damaging the rest of the code easily enough as well. 
	
Pieters and Wedel (2004) determined that the size of an image in an advertisement did not have a significant effect on the amount of time spent looking at the advertisement \cite{Pieters03}. They did, however, find that increasing the size of text in the ad resulted in significantly more fixations on the text. Similarly, Rayner, Rotello, Stewart, Keir and Duffy (2001) found that large text was the primary focus of the participants' viewing of an advertisement, as measured by fixation locations and lengths. As little as a 1\% increase of the text size was enough to produce significant differences in fixation time on the text, as opposed to the rest of the advertisement \cite{Rayner}. 
	
Pieters and Wedel (2012) show that the amount of time needed to view an advertisement, before it is recognized as an advertisement is under 100 milliseconds \cite{Pieters04}. If people can determine an image is an ad in under 100 milliseconds, it could be an external validity issue in research if participants are required to view advertisements for longer periods of time than this. In other words, people can determine if an image is an ad or not, and then choose to skip it, in far less than a second. For the present study, some participants will view images for only 100 milliseconds, while others view the same advertisements for different lengths of time. Then a comparison will be done to see if the amount of time spent viewing significantly changes how the image is viewed. The length of view time can also be factored into the creation of the GA. The amount of time spent viewing could be entered as a parameter of the program at run-time, and it would be up to the user to determine how long people would spend viewing the advertisement. 
	
Pieters, Wedel and Zhang (2007) looked at five specific elements of advertisements to see what caught people's attention. Specifically, Pieters et al. concluded that an increase in brand logo size, price size, and promotion size are all generally beneficial, while a decrease in text and picture size also tends to help for print ads \cite{Pieters05}. Interestingly enough, this study does not line up exactly with those of the Pieters and Wedel (2004) study, which found that greater text size resulted on more time spent viewing it. In the Pieters, Wedel and Zhang (2007) study however, they recommended reducing text size for more time spent viewing the ad. Maybe there is an optimum text size that both draws people to read it, but not to avoid the ad altogether. When looking at webpages, people are very good at ignoring advertisements or anything that could be mistaken as an advertisement \cite{Hsieh2011935}. This idea of banner blindness is mostly looked at for advertisements on websites, such as internet banners, but the present study examines the context print advertisements as well. 
	
If people are so good at ignoring advertisements, what would cause a person to attend to an ad? According to Ha and McCann (2008), advertisements which are viewed as providing useful information about the product, or ads that are entertaining, will be viewed positively, and will cause people to attend to them more frequently \cite{Ha}. Providing information that consumers will find useful in some way, or entertaining adds a new level to the development of an advertisement. Simply putting an aesthetically pleasing picture and some product information may not be enough to generate interest. People also tend to pay less and less attention to ads on subsequent pages within a website/web search. As more links are followed, ads are less likely to be attended to \cite{Hsieh:2012:RVA:2336819.2336888}.
	
When considering what factors to use when designing the fitness function for the present study's genetic algorithm, regions of interest (ROIs) are important to think about because these are the areas that people look at most when given an image. On a heat-map, these regions would be a much brighter red, than surrounding areas which received little to no attention. The goal of the present study is to attempt to replicate human ROIs (hROIs) with algorithmically generated ROIs (aROIs). Privitera and Stark (2000) used their own algorithm to simulate the fixation points and regions of interest for images. They used a different type of algorithm, but showed that regions of interest can be artificially generated, instead of only gotten from human participant data \cite{Privitera}.  A complication of their research was determining a scanpath that is predictable between participants and trials. Though for the sake of the present study, scanpath is not important because the order in which people jump from one point on the ad to another is not being looked at. In order to represent ROIs, the algorithm will have to determine how large to make them, and where to place them. Fortunately, the exact pixel coordinates on the horizontal and vertical planes are not very relevant. For example, if a person is looking at a face, and focusing on the mouth, the exact point of the mouth that is in focus isn't as relevant as the fact that the mouth is the focal point as a whole \cite{1999}. This is due to the way the fovea functions in the eye. As the fovea centers on a feature of a scene or image, that comes into focus, but the rest of the scene or image is not ignored. As long as the aROI generated covers the same space as the hROIs, it will be sufficient. However, the aROI cannot simply cover the whole advertisement, it must discriminate areas that are not of interest, while covering the areas that are. 
	
Once a ROI is determined, the size of the heat-map must be determined, which is an interesting challenge. When looking at a point in a visual field, how much does a person really focus on? Anything inside the fovea should be in focus, but as the image moves towards the periphery, it begins to lose focus, as there are less photo-receptors in the periphery than in the fovea \cite{Rybak}.

\vspace*{-.2in}
\section{Method of Approach}
\label{sec:method}
\vspace*{-.1in}

The eye tracking data for this research will be collected using the ASL Eye-Trac 6 Series Monocular eye tracker (Figure 1). The eye-tracker fits on the head like a baseball cap, and is tightened in the back, by the reseacher. The black aparatus above the left eye of the man in Figure 1 shoots an infrared laser down, which is reflected into the participant's eye by the glass plate below it. This is not harmful at all to the participant, nor is it noticable. From there, the eye is calibrated to the screen using a 9 point calibration system, and then the research can begin. 

\begin{figure}
\centering
\includegraphics[width=.5\columnwidth]{ASL_EyeTracker.JPG} 
\vspace*{.05in}
\caption{Headgear of an eye-tracking unit}
\end{figure}

In order to gather the data needed to create and test the GA, a 2x2 between-subjects model experiment will be used. The experiment will have the following setup:

\begin{itemize}
{\bf \item Independent Variables:}
   \begin{itemize}
   \item Instructions:
      \begin{itemize}
      \item Search based v. Control
      \end{itemize}	
   \end{itemize}

   \begin{itemize}
   \item Type of Advertisement:
      \begin{itemize}
      \item Internet banner v. Print advertisement
      \end{itemize}	
   \end{itemize}
{\bf \item Dependent Variables:}
   \begin{itemize}
   \item Post-viewing test scores
   \item Eye-tracking data
   \end{itemize}
\end{itemize}

The participants, upon arrival to the experiment, will be given a consent form to read and sign, signifying that they understand what will be asked of them in the research, and that they know they can leave at any time without having to provide a reason. The participants will be randomly assigned to one of the four possible conditions. Search instructions with Internet banner ads, search instructions with print ads, control group instructions with Internet banner ads, and finally control group instructions with print ads. Each condition will need between 12-15 participants, so in total I will need approximately 60 participants. Recruitment will be done, with prior permission of the instructor, in introductory psychology and computer science classes at Allegheny College. 

The search based instructions will ask participants to pay close attention to detail in the advertisements, as there will be a quiz at the end of the viewing session to test how much of the material they can remember. These instructions attempt to alter the goal of how participants are viewing the ads, with the hypothesis that different goals of viewing the ads will produce different results in the ways participants view the ads. 

The control instructions will simply ask the participants to view the advertisements as they normally would, and will not mention the post-viewing quiz. This is an attempt to simulate normal viewing of advertisements. 

Internet banner (Figure 2) and print advertisement (Figure 3) conditions are manipulations of what types of advertisements the participants will be viewing. These are different styles of advertisements, that are fairly exclusive to different mediums, so they are important to test the algorithm with. Each condition will have 30 different advertisements to provide a great deal of variety with which to test the algorithm. 

\begin{figure}
\centering
\includegraphics[width=.7\columnwidth]{banner.jpeg} 
\vspace*{.05in}
\caption{An example of an Internet banner}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=.5\columnwidth]{mcdonalds.jpeg} 
\vspace*{.05in}
\caption{An example of a print advertisement}
\end{figure}

After viewing the advertisements, the participants will be asked to take the post-viewing quiz to test how much of the material they remember. The main effects and interactions predicted for the experiment are as follows:

{\bf Main Effects:}
	\begin{itemize}
	\item The type of advertisement will have a main effect on post-viewing test scores. Particpants viewing print based advertisements will have a better memory for them in the post test than participants viewing Internet banners.
	\item The type of instructions will have a main effect on post viewing test scores, in that participants who are in the search based conditions will perform better in an assessment on their memory of the ads.
	\end{itemize}
	{\bf Interaction:} 
	\begin{itemize}
	\item The interaction of type of advertisement and type of instruction will show that participants in the search based condition, viewing print based ads will score the best on the post-test than any other condtion.
	\end{itemize}

Once the data has been collected, the GA itself can be worked on and tested to see if it is able to produce results similar to that of the participants' heat maps for the various advertisements. A fitness function will need to be created, and most likely will model the one used by Y. Zhang and his colleagues in their 2009 study. 

\vspace*{-.2in}
\section{Evaluation Strategy}
\label{sec:evaluate}
\vspace*{-.1in}

Once the algorithm is completed, it will be run on each of the images that participants viewed during the experiment. Heat maps will be generated for each image, and compared to the heat maps generated by the human participants. This comparison will be the evaluation of the success of the algorithm. If there is no significant difference between the two sets of heat maps, for all advertisements, then it can be said that the algorithm has successfully mimiced how people view advertisements. 

\vspace*{-.1in}
\section{Research Schedule}
\label{sec:schedule}
\vspace*{-.1in}

This will be a two semester comp project, with the first semester being primarily research and setup for the experimentation, programming and testing done in the second semester. It is estimated that finding advertisements, and programming the experiment in E-prime, which is the programming language used to control the eye tracker will be completed before the second semester. For the first two weeks of the second semester, any additional setup needed to begin research will be completed. Weeks 3-6 of the second semester will consist of testing participants and gathering data in the eye-tracking lab. Weeks 4-10 will consist of developing, and testing the algorithm, with the remaining weeks in the semester left for an analysis of results and writing up the report. 

Flexibility is key for this research, as the eye-tracking equipment is fragile, and will be in use by several people for their own projects. With this in mind, the schedule may need to be adjusted as the semester goes along, and this simply represents a preliminary attempt at estimating the schedule in an ideal world. 

\vspace*{-.1in}
\section{Conclusion}
\label{sec:conclusion}
\vspace*{-.1in}

With evidence that it is possible to model how people view simple objects and scenes, it appears that one of the logical next steps is to see what else this research can be applied to. Adverisements are a form of visual stimulus that everyone is confronted with on a daily basis. That being said, the amount of money spent on advertisements is in the billions of dollars, so the efficiency of these advertisements is very important. As of now, very few if any advertisers test their ads with the use of eye-tracking equipment. This is likely due to how expensive the equipment is, how much training is required to get a person prepared to use the equpiment, and the fact that only one person can be tested at a time. Instead, surveys are used by most advertisers to test their ads, by asking people what they liked and disliked about the ads, and whether or not they would be likely to buy the product. 

The goal of this research is to produce an algorithm that is able to simulate doing eye-tracking research on advertisements, without having to use the equipment, or human participants. This would allow advertisers to test their ads, during the design process, and make modifications as necessary, as to increase the efficiency of the ads. I believe this program would be higly desirable by advertising agencies everywhere. 

A 2x2 between-subjects experiment will be used to collect the data that is to be used for the building and testing of the genetic algorithm. This study will use different instruction types, and advertisement types to explore diffent possibilities when viewing ads. With this data, the algorithm will be designed and tested, and if the algorithm is successful it will be worked on further, and eventually marketed to advertisement agencies. 

\newpage
\bibliographystyle{plain}
\bibliography{finalpaper}

\end{document}

